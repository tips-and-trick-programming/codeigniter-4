# app/Models

## Template untuk membuat model :

[//]: <> (Diambil dari dokumentasi CI 4)
```php
<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
```

> Isikan property yang memang diperlukan saja, selebihnya akan diisi dengan value default dari CI

> Parent dari tiap model ada di file `system/Model.php`

## Menggunakan Model

1. Didalam controller :
	```php
	use App\Models\MahasiswaModel;
	.....
	$this->model = new MahasiswaModel();
	```
