# Setup

Setting CI_ENVIRONMENT di file .env folder root ke mode development

```
CI_ENVIRONMENT = development
```

# Start Project

Default : Langsung akses ke url/public

Menggunakan spark :

```bash
php spark serve
```

Hosting :

1. Copy file pada folder /public ke folder root (/)

2. Rubah realpath pada file index.php

   ```php
   $pathsPath = realpath(FCPATH . '../app/Config/Paths.php');
   $pathsPath = realpath(FCPATH . 'app/Config/Paths.php');
   ```

3. Tambahkan line berikut pada file app/Config/Constants.php

   ```php
	$protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
	$protocol .= "://".$_SERVER['HTTP_HOST'];
	$protocol .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
	defined('BASE') || define('BASE',$protocol);
   ```
   
   > Bisa juga dilakukan pada file .env `app.baseURL = ''`
   
4. Rubah $baseURL pada file app/Config/App.php

   ```php
   public $baseURL = BASE;
   ```

5. Langsung bisa diakses dari url root
