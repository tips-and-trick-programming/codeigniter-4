# CodeIgniter RestServer

Bagian ini adalah modifikasi code `RestController.php` dari library [CodeIgniter RestServer](https://github.com/chriskacerguis/codeigniter-restserver).

Saya memodifikasinya karna dalam penggunaan library ini saya mendapatkan beberapa error terkait kode `HTTP response`.

[RestController.php](./code/RestController.php)
