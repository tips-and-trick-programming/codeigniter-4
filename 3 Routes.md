# app/Config/Routes.php

Disarankan pakai route untuk membuat nama URL lebih mudah diingat dibanding mengikuti aturan segment PHP MVC (url/controller/method)
Semua jalur atau rute dari aplikasi dapat diatur menggunakan `route` ini (Seperti yang ada pada Laravel)

## Penggunaan Placeholder

Contoh code dibawah ketika kita mengakses url product/ dengan mengoper string berupa angka `(:num)` maka akan memanggil controller `Catalog` method `productLookup` dan dioper string berupa angka tadi.

```php
$routes->add('product/(:num)', 'Catalog::productLookup/$1');
```

[//]: <> (Diambil dari dokumentasi CI 4)
<table border="1"><colgroup><col width="8%"><col width="92%"></colgroup><thead valign="bottom"><tr class="row-odd"><th class="head">Placeholders</th><th class="head">Description</th></tr></thead><tbody valign="top"><tr class="row-even"><td>(:any)</td><td>will match all characters from that point to the end of the URI. This may include multiple URI segments.</td></tr><tr class="row-odd"><td>(:segment)</td><td>will match any character except for a forward slash (/) restricting the result to a single segment.</td></tr><tr class="row-even"><td>(:num)</td><td>will match any integer.</td></tr><tr class="row-odd"><td>(:alpha)</td><td>will match any string of alphabetic characters</td></tr><tr class="row-even"><td>(:alphanum)</td><td>will match any string of alphabetic characters or integers, or any combination of the two.</td></tr><tr class="row-odd"><td>(:hash)</td><td>is the same as <strong>(:segment)</strong>, but can be used to easily see which routes use hashed ids (see the <a class="reference internal" href="../models/model.html"><span class="doc">Model</span></a> docs).</td></tr></tbody></table>

> Saat menggunakan placeholder maka aturan URL Segment pada PHP MVC jadi terabaikan, sehingga perlu membuat routes yang lain untuk menghandle akses terhadap method tertentu
