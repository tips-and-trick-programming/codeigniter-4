# app/Controllers/BaseController.php

File ini adalah parrent dari controller lainnya, dan biasanya pada file ini akan digunakan untuk menload library, helper, variable, dll, yang bisa digunakan pada semua controller lainnya. Hal tersebut dilakukan didalam method `initController`

```php
$this->web_identity = [
  'title' => 'Nama Lengkap Dari WebApps',
  'title_short' => 'Nama Singkatnya',
  'version' => '1.0',
  'footer' => 'Footer Dari WebApps'
];
```

## Controller di dalam folder / namespace

Misalkan ingin meletakkan controller Users didalam folder Admin, jadi terdapat file `app/Controllers/Admin/Users.php`. Untuk melakukan itu perlu dilakukan sedikit perubahan didalam controller sebagai berikut agar tidak terdapat error :

```php
namespace App\Controllers\Admin;
use App\Controllers\BaseController;
```

Untuk mengakses controller tersebut maka harus menggunakan `routes` contohnya adalah sebagai berikut :

```php
$routes->get('/users', 'Admin\Users::index');
```
