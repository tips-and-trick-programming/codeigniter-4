# app/Views

## Memanajemen view agar lebih rapi

1. Pada file template, buat seperti berikut :

	```php
	<!DOCTYPE html>
	<html lang="en">
	<head>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <title>Document</title>
	</head>
	<body>
	
	  <?= $this->renderSection("content"); ?>
	
	</body>
	</html>
	```
	
	> $this->renderSection("content"); akan diisi dengan section content

2. Pada file pages yang akan diload, buat seperti berikut :

	```php
	<?= $this->extend("layout/template"); ?>
	
	<?= $this->section("content"); ?>
	<h1>Hello World</h1>
	<?= $this->endSection(); ?>
	```
	
	> $this->extend("layout/template"); menerangkan bahwa file ini adalah turunan dari file "layout/template"
	
	> semua yang diapit dengan code $this->section("content"); dan $this->endSection(); akan muncul pada $this->renderSection("content");

3. Didalam template biasanya agar lebih rapi maka navbar, footer, dll dibuat pada file terpisah. Untuk memanggil file terpisah tersebut bisa dilakukan dengan cara :

	```php
	<?= $this->include("menuju/file"); ?>
	```
