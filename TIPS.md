# Tips and Trick

- Pada CI4 sudah bisa menggunakan `d()` atau `dd()` dibandingkan `var_dump()`
- Cara menggunakan constructor :

	```php
	public function __construct()
	{
	  ..........
	}
	```
	
	> Tidak perlu mengisi `parent::__construct();` karna di parrent tidak ada fungsi tersebut, jadi bukan override parrent, tapi memang dibuat baru
